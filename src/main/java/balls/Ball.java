package balls;

import java.util.Random;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;



public class Ball{

	
	private Paint color;
	
	private double radius;
	
	private Motion xMotion;
	
	private Motion yMotion;
	
	private int steps = 0;
	
	private double bounceFactor = 0.99;

	/**
	 * Create a new ball with position and velocity (0,0)
	 * 
	 * @param color
	 *            The color
	 * @param radius
	 *            The radius
	 */
	public Ball(Paint color, double radius) {
		if (radius < 0)
			throw new IllegalArgumentException("Radius should not be negative");
		this.color = color;
		this.radius = radius;
		this.xMotion = new Motion();
		this.yMotion = new Motion();
	}

	/**
	 * @return Current X position
	 */
	public double getX() {
		return xMotion.getPosition();
	}

	/**
	 * @return Current Y position
	 */
	public double getY() {
		return yMotion.getPosition();
	}
	
	/**
	 * @return The ball's radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @return The ball's width (normally 2x {@link #getRadius()})
	 */
	public double getWidth() {
		return radius * 2;
	}

	/**
	 * @return The ball's height (normally 2x {@link #getRadius()})
	 */
	public double getHeight() {
		return radius * 2;
	}

	/**
	 * @return Paint/color for the ball
	 */
	public Paint getColor() {
		return color;
	}

	/**
	 * Number of steps is used to determine the behavior of the ball
	 * 
	 * @return
	 */
	public int getSteps() {
		return steps;
	}

	/**
	 * Move ball to a new position.
	 * 
	 * After calling {@link #moveTo(double, double)}, {@link #getX()} will return
	 * {@code newX} and {@link #getY()} will return {@code newY}.
	 * 
	 * @param newX
	 *            New X position
	 * @param newY
	 *            New Y position
	 */
	public void moveTo(double newX, double newY) {
		this.xMotion.setPosition(newX);
		this.yMotion.setPosition(newY);
	}
	
	/**
	 * @return Current X movement
	 */
	public double getDeltaX() {
		return this.xMotion.getSpeed();
	}

	/**
	 * @return Current Y movement
	 */
	public double getDeltaY() {
		return this.yMotion.getSpeed();
	}
	
	/**
	 * Perform one time step.
	 * 
	 * For each time step, the ball's (xPos,yPos) position should change by
	 * (deltaX,deltaY).
	 */
	public void move() {
		xMotion.move();
		yMotion.move();
		steps++;
	}
	
	/**
	 * This method makes one ball explode into 8 smaller balls with half the radius
	 * @return the new balls after the explosion
	 */
	public Ball[] explode() {
		// small balls will not explode
		if(getRadius()<8)
			return new Ball[0];
		
		
		Ball[] balls = new Ball[8];
		double radius = this.getRadius()/2;

		
		double[] randomSpeedX = getRandomSpeed(8);
		double[] randomSpeedY = getRandomSpeed(8);
		
		for(int i = 0; i < 8; i++) {
			Ball b = new Ball(this.getColor(), radius);
			b.moveTo(this.getX(), this.getY());
			b.accelerate(this.getDeltaX(), this.getDeltaY());
			b.accelerate(randomSpeedX[i],randomSpeedY[i]);
			balls[i] = b;
		}
		return balls;
	}

	/**
	
	 * @param size number of elements in the list
	 * @return a list of random values
	 */
	private double[] getRandomSpeed(int size) {
		
		double[] randSpeed = new double[size];
		double sum = 0.0;

		for(int i=0; i<size; i++) {
			randSpeed[i] = getRandomSpeed();
			sum +=randSpeed[i];
		}
		double avg = sum/size;
		for(int i=0; i<size; i++) {
			randSpeed[i] -= avg;
		}
		
		return randSpeed;
	}

	private double getRandomSpeed() {
		return Math.random()*2 - 1.0;
	}

	/**
	 * Changes the speed of this ball according to the rules for movement
	 * @param xAcceleration The extra speed along the x-axis
	 * @param yAcceleration The extra speed along the y-axis
	 */
	public void accelerate(double xAcceleration, double yAcceleration) {
		xMotion.accelerate(xAcceleration);
		yMotion.accelerate(yAcceleration);
	}

	/**
	 * Stops the motion of this ball
	 */
	public void halt() {
		setSpeed(0,0);
	}

	public void setSpeed(double xSpeed, double ySpeed) {
		xMotion.setSpeed(xSpeed);
		yMotion.setSpeed(ySpeed);
	}

	public void setAcceleration(double xAcc, double yAcc) {
		this.xMotion.setAcceleration(xAcc);
		this.yMotion.setAcceleration(yAcc);
		
	}

	public void setLowerLimitX(double limit) {
		xMotion.setLowerLimit(limit);
	}
	public void setLowerLimitY(double limit) {
		yMotion.setLowerLimit(limit);
	}
	public void setUpperLimitX(double limit) {
		xMotion.setUpperLimit(limit);
	}
	public void setUpperLimitY(double limit) {
		yMotion.setUpperLimit(limit);
	}
}
    
